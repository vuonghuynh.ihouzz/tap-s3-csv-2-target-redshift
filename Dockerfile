FROM python:3.7-slim-buster

RUN apt update && apt install -y gcc

RUN pip install pipelinewise-tap-s3-csv==1.2.2

RUN pip install pipelinewise-target-redshift==1.6.0

ENTRYPOINT ["target-redshift"]